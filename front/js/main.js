import App from './view/app.js';

(() => {
    window.addEventListener('DOMContentLoaded', () => {
        const app = new App({
            rootEl: document.getElementById('app')
        });
    })

})()