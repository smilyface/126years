import Component from '../system/component.js';
import * as Config from '../system/config.js';
import Router from '../system/router.js';
import Menu from './parts/menu.js';
import Years from './parts/years.js';
import Graph from './parts/graph.js';

import DataStorage from '../system/dataStorage.js';

import Admin from './parts/admin.js';

export default class App extends Component {
    constructor(options) {
        super('app')
        this.rootEl = options.rootEl;
        this.router = new Router();
        this.dataStorage = new DataStorage();
        this.render();

        window.dispatchEvent(new CustomEvent(Config.EVENTS.appReady));
        
        window.addEventListener('keydown', (e) => {
            // временно на рефреш, сервер не знает роутов
            (e.which == 116 || e.which == 17) ? location.href = '/': ''
            e.preventDefault();
        });
    }

    render() {
        this.append(
            Component.makeEl(`${Config.APPNAME}__h3`, 'h3').append(Component.makeTxt('Архив метеослужбы')),
            new Menu({
                router: this.router
            }),
            new Years(),
            new Graph({
                dataStorage: this.dataStorage
            }),

            new Admin()
        )

        this.appendTo(this.rootEl)
    }
}