import Component from '../../system/component.js';
import * as Config from '../../system/config.js';

export default class Menu extends Component {
    constructor(options) {
        super('menu');
        this.router = options.router;
        this.menuItems = [];
        this.render();
        this._setListeneres();
    }

    render() {
        Config.ROUTES.forEach((item) => {
            this.append(new MenuItem({
                name: item.name,
                item
            }))
        })
    }

    _setListeneres() {
        this.el.addEventListener('click', this._menuClickHandler.bind(this));
    }

    _menuClickHandler(e) {
        console.log(e.target.dataset.href);
        if (!e.target.dataset.href || e.target.dataset.href == this.router.currentRoute.name) return false
        this.router.changeLocation(e.target.dataset.href)
    }
}

class MenuItem extends Component {
    constructor(options) {
        super(`menu_item-${options.name}`)
        this.item = options.item
        this.render()
        this._setListeneres();
    }

    render() {
        this.el.dataset.href = this.item.name;
        this.el.append(Component.makeTxt(this.item.text))
        if (!this.item.base) this.toggleClass('active')
    }

    _setListeneres() {
        window.addEventListener(Config.EVENTS.locationChanged, (e) => {
            this.toggleClass('active')
        })
    }
}