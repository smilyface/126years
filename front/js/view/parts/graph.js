import Component from '../../system/component.js';
import * as Config from '../../system/config.js';
const COLOR = '#f00';
const COLOR2 = '#00f';
const COLOR_COORDS = '#000';

export default class Menu extends Component {
    constructor(options) {
        super('graph');
        this.dataStorage = options.dataStorage;
        this.statusText = this.dataStorage.text || '';
        this.render();
        this.initCanvas();
        this._setListeneres()
    }

    render() {
        this.statusBlock = Component.makeEl('status-text').append(Component.makeTxt(this.statusText));
        this.canvas = Component.makeEl('canvas', 'canvas').append(Component.makeTxt(`канвас`));
        this.dpi = window.devicePixelRatio;
        this.canvas.setAttribute('width', Config.CANVAS.width * this.dpi)
        this.canvas.setAttribute('height', Config.CANVAS.height * this.dpi)

        this.append(this.statusBlock, this.canvas)
    }

    initCanvas() {
        this.cx = this.canvas.getContext("2d")
        this.cx.scale(this.dpi, this.dpi);
        this.x0 = 2;
        this.y0 = 1;
        this.width = this.canvas.width;
        this.height = this.canvas.height / this.dpi;
    }

    _setListeneres() {
        window.addEventListener(Config.EVENTS.dataReady, () => {
            let data = this.dataStorage.currentDataForCanvas;
            this.averages = data.averages;
            this.yearsCoords = data.yearsCoordsArr;
            this.valuesCoords = data.valuesCoordsArr;

            this.draw();
        })
    }

    draw() {
        this.drawInit();
        this.drawCoords();
        this.drawGraph();
        // this.drawGraph2();
    }

    drawGraph() {
        //рисуем график
        this.cx.beginPath();
        // расчитываем смещение каждой точки графика по горизонтали
        this.xPerPixel = (this.stepX / (this.averages.length / (this.yearsCoords.length - 1))) / this.dpi;

        for (let i = 0; i < this.averages.length; i++) {
            let x = this.x0 + i * this.xPerPixel; //(this.stepX / this.dpi) * i;
            let y = this.y0 + this.stepY * Math.floor(this.valuesCoords.length / 2) -
                Math.floor(this.averages[i] * (this.stepY / Config.CANVAS.y_delta));

            if (0 == i) {
                this.cx.moveTo(x, y);
            } else {
                this.cx.lineTo(x, y);
            }
            //this.cx.arc(x, y, 2, 0, 2 * Math.PI, false);
            //this.cx.fillText(count, x-5, y-5);//текст над точками
            //this.cx.fillText(count, x0 - 15, y);//текст у боковой линии
        }

        this.cx.strokeStyle = COLOR;
        this.cx.lineWidth = 1;
        this.cx.stroke();
        this.cx.closePath();
    }

    drawInit() {
        // чистим, расчитываем шаги координат графика
        this.cx.clearRect(0, 0, this.width, this.height)
        this.stepY = Math.round(this.height / this.valuesCoords.length);
        this.stepX = Math.round(this.width / this.yearsCoords.length);
    }

    drawCoords() {
        this.cx.beginPath();
        //вертикаль
        this.cx.moveTo(this.x0, this.y0);
        this.cx.lineTo(this.x0, this.height + this.y0);
        //горизонталь
        let horY = this.y0 + this.stepY * Math.floor(this.valuesCoords.length / 2) // вертикальное смещение горизонтальной оси
        this.cx.moveTo(this.x0, horY)
        this.cx.lineTo(this.width + this.x0, horY);

        //горизонтальная разметка и цифры
        let m = 0;
        for (let i = this.x0; m < this.yearsCoords.length; i += this.stepX / this.dpi) {
            m++;
            this.cx.moveTo(i, horY);
            this.cx.lineTo(i, horY + 15);
            this.cx.fillText(this.yearsCoords[m - 1].year, i + 3, horY + 12);
        }
        //вертикальная разметка
        m = 0;
        for (let i = this.y0; m < this.valuesCoords.length; i += this.stepY) {
            m++;
            this.cx.moveTo(this.x0, i);
            this.cx.lineTo(this.x0 + 5, i);
            this.cx.fillText(this.valuesCoords[m - 1] != 0 ? this.valuesCoords[m - 1] : '', this.x0 + 2, i + 9);
        }

        this.cx.lineWidth = this.dpi == 1 ? 2 : 1;
        this.cx.strokeStyle = COLOR_COORDS;
        this.cx.stroke();
        this.cx.closePath();
    }

    drawGraph2(){

        this.xPerPixel = (this.stepX / (this.averages.length / (this.yearsCoords.length - 1))) / this.dpi;
        this.cx.strokeStyle = COLOR;
        let points = [];
        for (let i = 0; i < this.averages.length; i++) {
            let x = this.x0 + i * this.xPerPixel;
            let y = (this.y0 + this.stepY * Math.floor(this.valuesCoords.length / 2) -
                Math.floor(this.averages[i] * (this.stepY / Config.CANVAS.y_delta)));

            points.push([x, y]);
        }

        this.bezierCurveThrough(this.cx, points);
    }


    bezierCurveThrough(ctx, points, tension) {
    
        tension = tension || 0.05;
    
        let l = points.length;
        
        ctx.beginPath();
        ctx.moveTo(this.x0, this.y0);
    
        function h(x, y) {
            return Math.sqrt(x * x + y * y);
        }
    
        let cpoints = new Array(points.length).fill({})
    
        for (let i = 1; i < l - 1; i++) {
            let pi = points[i],    
                pp = points[i - 1],
                pn = points[i + 1];
    
            let rdx = pn[0] - pp[0],
                rdy = pn[1] - pp[1],
                rd = h(rdx, rdy),   
                dx = rdx / rd,      
                dy = rdy / rd;      
    
            let dp = h(pi[0] - pp[0], pi[1] - pp[1]),
                dn = h(pi[0] - pn[0], pi[1] - pn[1])

            let cpx = pi[0] - dx * dp * tension,
                cpy = pi[1] - dy * dp * tension,
                cnx = pi[0] + dx * dn * tension,
                cny = pi[1] + dy * dn * tension;
    
            cpoints[i] = {
                cp: [cpx, cpy],
                cn: [cnx, cny],
           };
        }
    
        cpoints[0] = {
            cn: [ (points[0][0] + cpoints[1].cp[0]) / 2, (points[0][1] + cpoints[1].cp[1]) / 2 ],
        };
        cpoints[l - 1] = {
            cp: [ (points[l - 1][0] + cpoints[l - 2].cn[0]) / 2, (points[l - 1][1] + cpoints[l - 2].cn[1]) / 2 ],
        };
    
        ctx.moveTo(points[0][0], points[0][1]);
    
        for (let i = 1; i < l; i++) {
            let p = points[i],
                cp = cpoints[i],
                cpp = cpoints[i - 1];

            ctx.bezierCurveTo(cpp.cn[0], cpp.cn[1], cp.cp[0], cp.cp[1], p[0], p[1]);
        }
        ctx.strokeStyle = 'black';
        ctx.lineWidth = 1;
        ctx.stroke();
        ctx.closePath();
    }
}