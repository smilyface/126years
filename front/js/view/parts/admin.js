import Component from '../../system/component.js';
import * as Config from '../../system/config.js';

export default class App extends Component {
    constructor(options) {
        super('admin')
        this.render();
        this._setListeners();
    }

    render() {
        this.reloadButton = Component.makeEl('button').append(Component.makeTxt('reload'));
        this.clearButton = Component.makeEl('button').append(Component.makeTxt('clear'));
        this.clearAndReloadButton = Component.makeEl('button').append(Component.makeTxt('clear & reload'));
        this.append(
            Component.makeEl('text').append(Component.makeTxt('admin')),
            // this.reloadButton,
            this.clearButton,
            this.clearAndReloadButton
        )
    }

    _setListeners() {
        // this.reloadButton.addEventListener('click', this.reloadClickHandler)
        this.clearButton.addEventListener('click', this.clearClickHandler)
        this.clearAndReloadButton.addEventListener('click', this.clearAndReloadClickHandler.bind(this))
    }

    clearClickHandler() {
        return new Promise((res, rej) => {
            let request = indexedDB.deleteDatabase(Config.DB_NAME);
            request.onsuccess = () => {
                console.log("Deleted database successfully");
                res()
            };
            request.onerror = function () {
                console.log("Couldn't delete database");
                rej()
            };
            request.onblocked = function () {
                console.log(request);
                console.log("Couldn't delete database due to the operation being blocked");
                rej()
            };
        })

    }

    clearAndReloadClickHandler() {
        this.clearClickHandler().then(() => {
            this.reloadClickHandler()
        })
    }

    reloadClickHandler() {
        location.href = '/'
    }
}