import Component from '../../system/component.js';
import * as Config from '../../system/config.js';

export default class Years extends Component {
    constructor() {
        super('years');
        this.setMinMax()
        this.render();
        this._setListeners();
    }

    setMinMax() {
        this.min = Config.firstYear;
        this.max = Config.lastYear;
    }

    _setListeners() {
        window.addEventListener(Config.EVENTS.locationChanged, (e) => {
            // при смене роута min и max ставим в базовые значения
            this.setMinMax();
            this.childrenRecover()
        })
    }

    render() {
        this.select1 = new YearSelect({
            isMin: true,
            parent: this
        })
        this.select2 = new YearSelect({
            isMin: false,
            parent: this
        })
        this.append(this.select1, this.select2)
    }

    childrenRecover() {
        this.select1.recover()
        this.select2.recover()
    }
}

class YearSelect extends Component {
    constructor(options) {
        super('select')
        this.isMin = options.isMin;
        this.parent = options.parent;
        // min и max хранит родитель
        this.render()
        this._setListeners();
    }

    render() {
        this.select = Component.makeEl(`years_select`, `select`);
        Config.YEARS.forEach((year, i) => {
            this.select.append(this._renderOption(i));
        })
        this.append(this.select)
    }

    _renderOption(index) {
        return Component.makeEl('year_select_option', `option`).append(
            Component.makeTxt(Config.firstYear + index)
        );
    }

    _setListeners() {
        this.select.addEventListener('change', this._selectHandler.bind(this))
        window.addEventListener(Config.EVENTS.appReady, this._selectedIndexes.bind(this))
    }

    _selectedIndexes() {
        this.select.selectedIndex = this.isMin ? Config.getIndexOfYear(this.parent.min) : Config.getIndexOfYear(this.parent.max)
    }

    _selectHandler(e) {
        let newValue = Config.firstYear + e.target.selectedIndex;
        if (this.isMin) {
            if (newValue > this.parent.max) {
                alert('Минимальный год должен быть меньше или равен максимальному')
                this.recover();
                this.parent.setMinMax()
                this.minMaxChangedEvent()
                return false;
            }
        } else {
            if (newValue < this.parent.min) {
                alert('Максимальный год должен быть больше или равен минимальному')
                this.recover();
                this.parent.setMinMax()
                this.minMaxChangedEvent()
                return false;
            }
        }
        this.parent[this.isMin ? 'min' : 'max'] = Config.firstYear + e.target.selectedIndex;
        this.minMaxChangedEvent()
    }

    recover() {
        this.select.selectedIndex = this.isMin ? 0 : Config.YEARS.length - 1
    }

    minMaxChangedEvent(min = this.parent.min, max = this.parent.max) {
        console.log(min, max);
        window.dispatchEvent(new CustomEvent(Config.EVENTS.yearsChanged, {
            detail: {
                min,
                max
            }
        }));
    }
}