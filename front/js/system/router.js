import * as Config from './config.js';

export default class Router {
    constructor() {
        this.currentRoute = null;
        window.addEventListener(Config.EVENTS.appReady, this.init.bind(this))
    }

    init() {
        // с корня переходим на страницу температуры
        let firstRoute = Config.ROUTES.find(r => r.base);
        this.changeLocation(firstRoute.name)
    }

    changeLocation(path) {
        let route = Config.ROUTES.find(r => r.name == path);
        window.history['pushState'](route, route.text, path);
        this.currentRoute = route;
        window.dispatchEvent(new CustomEvent(Config.EVENTS.locationChanged, {
            detail: {
                route
            }
        }));
    }
}