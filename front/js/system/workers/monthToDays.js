onmessage = event => {
    const months = event.data.dbData;
    const years = event.data.years;
    const yearsStepsCount = event.data.canvasConfig.x_step_count;
    const valuesDelta = event.data.canvasConfig.y_delta;
    const DAYS_IN_YEAR = 365; // для простоты
    let stepX = null; //
    let result = {};
    let averages = [];
    let minValue = 0;
    let maxValue = 0;
    let length = 0;
    let yearsCoordsArr = []; // массив координат горизонтальной оси (года)
    let valuesCoordsArr = []; // массив координат вертиклаьной оси (значения)
    let yearsStep; // количество лет в каждом шаге шкалы
    if ((years.max - years.min) <= 10) {
        yearsStep = 1;
    } else {
        yearsStep = Math.ceil((years.max - years.min) / yearsStepsCount)
    }
    (() => {
        // соберем массив для горизонтальной шкалы
        for (let i = 0; i <= yearsStepsCount + 1; i++) {
            let yearsDelta = years.min + ((yearsStep) * i);
            let result = {
                year: yearsDelta > years.max ? (years.max + 1) : yearsDelta,
                days: yearsDelta > years.max ?
                    (years.max + 1 - years.max) * DAYS_IN_YEAR : yearsStep * DAYS_IN_YEAR,
            }

            if (yearsDelta > years.max) {
                yearsCoordsArr.push(result)
                break;
            } else {
                yearsCoordsArr.push(result)
            }
        }

        // столько пикселей влезает в один промемжуток по горизонтали
        stepX = Math.round(event.data.canvasConfig.width / yearsCoordsArr.length);
        yearsCoordsArr.forEach((coord, index) => {
            // console.log(coord, stepX);
            // количество дней, среднее значение с которых надо взять
            yearsCoordsArr[index].daysInAverages = (coord.days / stepX);
        })
    })();

    (() => {
        let ii = count = yearsCoordsArr[0].daysInAverages;
        let summ = 0;
        // найдем максимальное и минимальное значения для вертикальной шкалы
        // соберем средние значения для графика
        months.forEach((month, i) => {
            month.monthValues.forEach(day => {
                if (ii > 0) {
                    summ += day.v;
                    ii--;
                } else {
                    averages.push(Math.ceil(summ / count));
                    ii = count;
                    summ = 0;
                }
                minValue = minValue < day.v ? minValue : day.v;
                maxValue = maxValue > day.v ? maxValue : day.v;
                length++;
            })
        });

        let minRound = Math.floor(minValue / 10) * 10; // округляем до числа кратного 10
        let maxRound = Math.ceil(maxValue / 10) * 10; // округляем до числа кратного 10
        let maxCoordAbs = Math.max(Math.abs(minRound), Math.abs(maxRound))
        let valuesStepsCount = ((maxCoordAbs * 2) / valuesDelta);
        for (let i = -1 * maxCoordAbs; i <= maxCoordAbs; i += valuesDelta) {
            valuesCoordsArr.push(i)
        }
        valuesCoordsArr.reverse()
    })()

    result = {
        averages,
        length,
        yearsCoordsArr,
        valuesCoordsArr
    };

    postMessage(result);
};