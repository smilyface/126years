onmessage = event => {
    const DATE_KEY = "t";
    const VALUE_KEY = "v";
    const DATE_SEPARATOR = "-";
    const days = event.data;
    let months = {};

    for (let i = 0; i < days.length; i++) {
        let t = days[i][DATE_KEY].split(DATE_SEPARATOR);
        t.pop();
        let yearAndMonthKey = t.join(DATE_SEPARATOR);
        months[yearAndMonthKey] = months[yearAndMonthKey] || [];
        months[yearAndMonthKey].push(days[i]);
    }

    postMessage(Object.entries(months));
};