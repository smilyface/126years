import * as Config from './config.js';

export default class Db {
    constructor() {
        this._db = null;
    }

    open() {
        this.openRequest = indexedDB.open(Config.DB_NAME, 1);
        return new Promise((res, rej) => {
            this.openRequest.onupgradeneeded = event => this.onupgradeneeded(event.target.result);
            this.openRequest.onsuccess = e => {
                this._db = e.target.result;
                res(this)
            };
            this.openRequest.onblocked = function (e) {
                console.log(e);
            }
            this.openRequest.onerror = function (e) {
                rej(e)
                console.log(e);
            };
        })
    }

    close() {
        this._db.close()
    }

    getStore(name, readonly = false) {
        return this._db.transaction(name, readonly ? 'readonly' : 'readwrite').objectStore(name);
    }

    getData(route, years) {
        return new Promise((res) => {
            let tx = this.getStore(route, true);
            let arr = [];
            tx.openCursor(IDBKeyRange.bound(`${years.min}-01`, `${years.max}-12`)).onsuccess = (e) => {
                let cursor = e.target.result;
                if (cursor) {
                    arr.push(e.target.result.value)
                    cursor.continue()
                } else {
                    res(arr);
                }
            }
        })
    }

    onupgradeneeded(db) {
        console.log('db upgraded');
        db.onerror = function (event) {
            throw new Error('Error db upgrade');
        };
        if (!db.objectStoreNames.contains("temperature") || !db.objectStoreNames.contains("precipitation")) {
            db.createObjectStore('temperature');
            db.createObjectStore('precipitation');
        }

    }
}