import * as Config from './config.js';

export default class Api {
    loadJson(name) {
        return new Promise((res, rej) => {
            fetch(`${Config.API_PATH}${name}.json`)
                .then((response) => {
                    return response.json()
                })
                .then((data) => {
                    res(data)
                })
        })
    }
}