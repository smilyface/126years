import Api from './api.js';
import Db from './db.js';
import * as Config from './config.js';

export default class DataStorage {
    constructor() {
        this.currentRoute = null;
        this.currentDataForCanvas = null;
        this.years = {
            min: Config.firstYear,
            max: Config.lastYear
        };
        console.log(this.years);
        this.api = new Api();
        this.db = new Db();
        this._daysToMonth = new Worker(`${Config.DAYS_TO_MONTH_WORKER}`);
        this._monthToDays = new Worker(`${Config.MONTH_TO_DAYS_WORKER}`);
        this._setListeners();
    }

    _setListeners() {
        window.addEventListener(Config.EVENTS.locationChanged, (e) => {
            this.currentRoute = e.detail.route.name;
            this.years = {
                min: Config.firstYear,
                max: Config.lastYear
            };
            this._prepareData();
        })
        window.addEventListener(Config.EVENTS.yearsChanged, (e) => {
            this.years = {
                min: e.detail.min,
                max: e.detail.max
            };
            console.log(this.years);
            this._prepareData()

        })
    }

    _prepareData() {
        this.db.open().then((db) => {
            // todo проверять по наличию стора
            this.db.getStore(this.currentRoute).count().onsuccess = (e) => {
                // console.log(e.target.result);
                if (!e.target.result) {
                    this._loadData()
                } else {
                    // данные в базе есть, читем, отдаем канвасу
                    this._getDataFromDb()
                }
            }

        })
    }

    _loadData() {
        this.api.loadJson(this.currentRoute).then((data) => {
            this._workerConverter(this._daysToMonth, data).then((months) => {
                this._saveData(months).then((count) => {
                    console.log(count.result);
                    // данные в базе есть, читем, отдаем канвасу
                    this._getDataFromDb()
                });
            })
        })
    }

    _workerConverter(worker, data) {
        worker.postMessage(data)
        return new Promise((res) => {
            worker.onmessage = event => {
                res(event.data)
            };
        })
    }

    _getDataFromDb() {
        this.db.getData(this.currentRoute, this.years).then((data) => {
            this.db.close()
            this._workerConverter(this._monthToDays, {
                years: this.years,
                dbData: data,
                canvasConfig: Config.CANVAS
            }).then((newdata) => {
                this.currentDataForCanvas = newdata;
                this._dataReadyEvent();
            })
        })
    }

    _saveData(data) {
        let i = 0;
        return new Promise((res, rej) => {
            let tr = this.db.getStore(this.currentRoute);
            let saveOne = () => {
                if (i < data.length) {
                    tr.put({
                        monthValues: data[i][1]
                    }, data[i][0]).onsuccess = saveOne; // по готовности записи пишем следующую запись
                    i++;
                } else {
                    let count = this.db.getStore(this.currentRoute).count();
                    count.onsuccess = () => res(count);
                }
            }
            saveOne();
        })
    }

    _dataReadyEvent() {
        window.dispatchEvent(new CustomEvent(Config.EVENTS.dataReady, {
            detail: {}
        }));
    }
}