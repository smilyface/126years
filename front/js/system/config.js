export const APPNAME = 'weather';
export const ROUTES = [{
        base: true,
        id: 1,
        name: 'temperature',
        text: 'Температура',
    },
    {
        id: 2,
        name: 'precipitation',
        text: 'Осадки',
    }
];
export const YEARS = new Array(126).fill();
export const firstYear = 1881;
export const lastYear = firstYear + YEARS.length - 1;
export const getIndexOfYear = (year) => {
    return year - firstYear;
}

export const API_PATH = '/json/';
export const DB_NAME = 'weather_db';
const WORKER_PATH = '/js/system/workers/';
export const DAYS_TO_MONTH_WORKER = `${WORKER_PATH}daysToMonth.js`;
export const MONTH_TO_DAYS_WORKER = `${WORKER_PATH}monthToDays.js`;

export const CANVAS = {
    width: 540,
    height: 360,
    x_step_count: 10, // количество делений на графике
    y_delta: 5 // шаг графика
}

const STATUS = {
    READY: {
        text: 'Готово'
    },
    LOADING_JSON: {
        text: 'Загружаем json',
    },
    DB_CHECKING: {
        text: 'Проверяем базу на пустоту'
    },
    DB_WRITING: {
        text: 'Пишем в базу'
    },
    DB_READING: {
        text: 'Читаем из базы'
    }
}

export const EVENTS = {
    dataReady: 'dataReady',
    locationChanged: 'locationChanged',
    yearsChanged: 'yearsChanged',
    appReady: 'appReady',

}