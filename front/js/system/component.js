import {
    APPNAME
} from '../system/config.js';

export default class Component {
    constructor(name = `default`) {
        this._name = `${name}`;
        this.class = `${APPNAME}__${this._name}`;
        this.el = Component.makeEl(this.class);
    }
    // set data([dataName, dataVal]) {
    //     this.el.dataset[dataName] = dataVal;
    // }
    get el() {
        return this._el;
    }
    set el(_el) {
        if (_el) this._el = _el;
    }
    append(...elems) {
        Component.append(this.el, ...elems);
    }
    appendTo(target) {
        let el = target;
        if (target && !target.nodeType) {
            el = target.el || null;
        }
        if (el) {
            Component.append(el, this.el);
        }
        return this;
    }
    addClass(...names) {
        for (let name of names) {
            this.el.classList.add(`${name}`);
        }
    }
    removeClass(...names) {
        for (let name of names) {
            this.el.classList.remove(`${name}`);
        }
    }
    toggleClass(name, add = false) {
        if (add) {
            this.el.classList.add(`${name}`);
        } else {
            this.el.classList.toggle(name);
        }
    }
    html(html) {
        this.el.innerHTML = `${html}`;
    }
    remove() {
        this.el.remove();
    }
    static makeEl(name = false, tag) {
        let el = document.createElement(tag ? tag : 'div');
        if (name) el.className = name;
        el.append = (...elems) => {
            Component.append(el, ...elems);
            return el;
        };
        el.clearHtml = () => {
            el.innerHTML = '';
        };

        return el;
    }
    static append(target, ...elems) {
        var fragment = document.createDocumentFragment();
        for (let el of elems) {
            if (!el) continue;
            fragment.appendChild(el.el || el);
        }

        target.appendChild(fragment);
    }
    static makeTxt(val) {
        return document.createTextNode(val);
    }
}