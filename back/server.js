let http = require('http');
let fs = require('fs');

let server = http.createServer(function (req, resp) {
  if (req.url === "/") {
    readAndAnswer("../front/index.html", 'text/html', resp)
  } else if (req.url !== '/') {
    switch (req.url.slice(-3)) {
      case '.js':
        readAndAnswer("../front/" + req.url, 'application/javascript', resp)
        break;
      case 'css':
        readAndAnswer("../front/" + req.url, 'text/css', resp)
        break;
      case 'son':
        readAndAnswer("../front/" + req.url, 'application/json', resp)
        break;
    }
  }
});

function readAndAnswer(path, type, resp) {
  fs.readFile(path, function (error, pgResp) {
    if (error) {
      resp.writeHead(404);
      resp.write('Contents you are looking are Not Found');
    } else {
      resp.writeHead(200, {
        'Content-Type': type
      });
      resp.write(pgResp);
    }
    resp.end();
  });
}

server.listen(8887);

console.log('Server Started listening on 8887');