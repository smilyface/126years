# Тестовое задание "Архив погоды"

## Настройка
```bash
git clone git@bitbucket.org:smilyface/126years.git
```

если что-то не так с нодой или ее нет, то можно
поставить расширение для хрома https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb/reviews
и указать каталог ```126years/front``` для отдачи файлов приложения

если c нодой все хорошо, то 
запускаем нод таску 
```bash
cd 126years/back
node server.js
```

переходим http://127.0.0.1:8887/

## О приложении
Весь фронтенд основан на компонентах. Приложение само себя рендерит, и может перерендеривать гибко. Меню, фильтр лет, график -  все является компонентомами и может содержать в себе другие компоненты, каждый компоненты призван решать свою задачу. 
Это сделано для чистоты кода, гибкости логики, удобной расширяемости, хотя вроде не требовалось, но без этого уже неудобно. На мой взгляд, легкая избыточность оправдана удобством в уже таком, относительно малом приложении

#### Система событий
Я решил попробовать кастомные события(CustomEvent) вместо стандартного эмиттера, решение легло нормально, но можно улучшить.

#### Очередность
Весь рендер лежит в каталоге ```front/js/view```. Как только рендер прошел, готовится роутер ```front/system/router.js``` и хранилище данных ```front/system/dataStorage.js```. 
Роутер решает с чем мы работаем и что грузить в первую очередь. По условиям задачи - страница температуры. Хранилище данных, меню, график и филтр лет реагируют на смену роута. Сейчас нас интресует хранилище.
Оно работает с классами апи```front/system/api.js``` и базы данных```front/system/db.js```. 
Идет проверка наличия данных в базе, учитывая текущую страницу и грузит с сервера данные через апи. 
Хранилище запускает воркер```front/system/workers/daysToMonth.js``` для конвертации данных для базы(условия задачи). 
пишет, по готовности запускатся выборка, учитывая выбранные года фильтра. 
Запускается воркер```front/system/workers/monthToDays.js``` для конвертации данных для канваса```front/view/parts/graph.js```(условие задачи).
На изменение фильтра лет```front/view/parts/years.js``` или переход на другую страницу```front/view/parts/menu.js```, процедура провреки и подготовки данных повторяется, учитывая условия.

#### График
При обходе воркером данных ищем максимальное и минимальное значения для построения вертикальной шкалы. Горизонталную шкалу при высоком масштабе разбиваем на 10 секций по n лет в каждой и усредняем значения графика в этом промежутке(до скольких-то на пиксель).
при 126 годах выглядит как расчестка - это некрасиво.
на малых промежутках выглядит интереснее.
пробовал строить кривые(интерполировать), метод ```drawGraph2()``` в компопненте графика. но на малые пиксели по горизонтали не увидел красоты. надо тогда брать каждую n-ную точку и между ними строить кривые, но так мы потеряем часть данных.
График осадков имеет судя ао всему большие вершины, но те срезаются из-за усреднения, можно переделать вертикальную шкалу осадков для более красивого графика(взять для нее максимальное от среднего)
Вообще задача с графиком досттоачно глубока для экспериментов

### Что не так (для условного прода)
* Внешинй вид собран на коленке.
* Js надо конкатинировать все еще (импорты делают водопад загрузок).
* Запись данных в базу занимает определенную заметную паузу (хотя и не блочит).
* Еще больше смущает, что все операции в воркерах занимают даже на глаз сильно меньше времени, чем запись в базу. 
* Систему событий избавить от window(но тогда везде надо прокидывать рутовый елемент приложения) или переписать на емитер и прокидывать его.
* График требует улучшений.

### Как это было у меня
Наверное я двольно сильно увлекся самим созданием приложения(еще до графика), и оно, весьма неплохо получилось, но и надо признать, что это больше похоже на оттачивания скила. Работа с базой данных конечно требует контроля некоторых моментов, но по большей своей части прозрачна. 
График, вот тут большой простор для экспериметов и применения методик, к сожалению, тут не удалось опробовать всего и наверное нельзя называть сам график идеальным, думаю, его можно назвать ограниченно информативным. На данный момент, надо полагать, с графиком как раз и надо работать в плане интерполяции и апроксимации, но возможно, я пока не понимаю, какой именно результат(качественный) от этого графика я хочу получить